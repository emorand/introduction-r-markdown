---
title: "Tricoter"
author: "premier essai"
date: "02/06/2021"
output: 
  html_document:
    toc: true
    toc_float: true
    theme: flatly
    
---


Ceci est un tutoriel de montage de tricot , on y trouve les deux premiers types de mailles

- Maille endroit 
- Maille envert


Les étapes à suivre pour un premier tricot

1. on monte les mailles
1. on fait un rang endroit
1. On fait un rang envers
1. On rabat les mailles




 

# Etape 1 : monter les mailles

## Montage Traditionnel

### La première maille

On fait un noeud (_cri des puristes!_). Pour un beau tricot , il vaudrait mieux **ne plus** faire de noeud

### Les autres

### Inconvenient

Il faut bien maintenir la tension pour que se soit régulier

## Montage à l'anglaise



# Etape 2 : le point endroit

Exemple d'utilisation

| point         | description     | 
|:-------------:|:---------------:|
| jersey        | 1 rang endroit , 1 rang envers  | 
| point mousse  | tous les rangs endroits         | 
| point de goudron  | 2 rangs endroits , 2 rangs envers       |



# Etape 3 : le point envers

# Le point sur Tricot

![]()
[source](https://www.data.gouv.fr/fr/territories/commune/60643@latest/Tricot/)
