---
title: Le pire article

# to produce blinded version set to 1
blinded: 0

authors: 
- name: F. Hantomatique
  affiliation: University of Melon, Melon, France
  
- name: Otto R.Fanthome
  affiliation: Department of Null University

keywords:
- article
- fantome


abstract: |
  ceci n'est pas un article.
  C'est pour donner une structure à une idée !

bibliography: [packages.bib, bibliography.bib]
output: rticles::asa_article
---

```{r, echo=FALSE, message=FALSE, warning=FALSE}


source("http://forge.info.univ-angers.fr/~gh/statgh.r",encoding="latin1")

titanic <- lit.dar("http://www.info.univ-angers.fr/~gh/Datasets/titanic.dar")

#Et on ajoute une données supplémentaire factice

set.seed(300)
titanic$age<-rnorm(n=nrow(titanic),mean=20,sd=2)



```

```{r, include=FALSE}
knitr::write_bib(file = 'packages.bib')
```

# Introduction

Faute d'écrire un article on vous renvoie ici au merveilleux ,
"le meilleur article de tous les temps"
http://www.mimiryudo.com/blog/2020/08/le-meilleur-article-de-tous-les-temps/?fbclid=IwAR0Ma0JYrdfg7kmbtf6qldhmZ8Pg5dFzb9qXmcTVD64D8pGVxXaK_dQwgoo

On a repris des bouts de l'article pour remplir le notre!!

# MATERIALS AND METHODS
## Study  1 –Relationship  between  the Use of HCQ + AZT and Frequency of PSA in France in 2020}
\label{sec:verify}
Our  objective  was  to  evaluate  the  relationship between PSA and usage of hydroxychloroquine (HCQ) in France in 2020.The  computers  and  the  Internet  services  were provided by the authors or by private institutions exclusively.  The  state  of  public  research  in France is not really compatible with the purchase of  equipment  quickly  (except  after  justifying needs with the help of twelve forms, a photocopy of the rental lease of your student room from 18 to  25  years  old  and  your  car  registration,  in  3 copies).  We  used  data  from  OpenMEDIC (https://www.data.gouv.fr/fr/datasets/open-medic-base-complete-sur-les-depenses-de-medicaments-interregimes/) to  determine  usage of  hydroxychloroquine  in  France  in  2020.  We used  Google  Actuality  to  determine  the  rate  of PSA  in  French  press  in  2020  (query  equation: “accidents de trottinette”). We have not classified the  accidents  by  type,  date  or  anything  else, essentially by laziness.
@Campbell02 @Schubert13 [@Chi81]




# RESULTS

## Study  1 –Relationship  between  the Use of HCQ + AZT and Frequency of PSA in France in 2020

On Google Actuality (page 1), we noted 1 PSA on  20th  July  (Val  d’Oise),  1  on  20th  October 2019(Bordeaux), 2 on 2th and 22th september 2019  (Reims  and  Levallois-Perret),  1  on  26th april  2020  (Nord-sur-Edre),  1  on  2th  december 2019  (Nancy)  and  1  on  20th  january  2020 (Villefranche-sur-Saône).

As our results didn’t find PSA in March 2020, we concluded  that  hydroxychloroquine  was  an effective preventive therapy for PSA with a RR = 0 (p<0.0001).We only  consulted  page  1  of  Google  Actuality: following  the  methodological  rule  according  to which  the  smaller  the  sample,  the  higher  the significance   [11],   we   decided   to   stop           recruitment  as  soon  as  we  found  a  significant effect.

## les femmes et les enfants d'abord

Répartition par classes de passagers

```{r, echo=FALSE}



library(forcats)



titanic$CLASS_c1<- fct_recode(as.factor(titanic$CLASS),
           "Equipage" = "0",
           "class1-2" = "1",
           "class1-2"="2",
           "classe3" ="3")




tab <- as.data.frame(xtabs(~CLASS_c1, titanic))

dotchart(tab$Freq, xlim = c(0, max(tab$Freq)), pch = 21, bg = c("orangered","black","chocolate4"), cex = 1.5,labels=tab$CLASS_c1)


```

